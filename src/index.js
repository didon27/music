import ReactDOM from "react-dom";
import React from "react";
import "./styles.css"

const track = require.context('./songs/', true, /mp3$/)
const keys = track.keys()
const list = keys.map(key => ({
  path: key,
  file: track(key),
}));


class Music extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      play: false,
      pause: true,
      count: 0,
    }
    this.audio = new Audio(list[this.state.count].file);

  }


  play = () => {
    this.setState((state) => {return { play: true, pause: false }})
    this.audio.play();
    
  }

  pause = () => {
    this.setState((state) => {return{ play: false, pause: true }})
    this.audio.pause();
  }

  next = () => {
    this.audio = new Audio(list[this.state.count].file);
    this.audio.play();
    this.setState((state) => {return{ count: this.state.count + 1 }});
  }

  previous = () => {
    this.audio = new Audio(list[this.state.count].file);
    this.audio.play();
    this.setState((state) => {return{ count: this.state.count - 1 }})
  }


  render() {
    return (
      <div className="container">
        <div className="block">
          <p>{keys}</p>
          <button onClick={this.previous}>Previous</button>
          <button onClick={this.play}>Play</button>
          <button onClick={this.pause}>Pause</button>
          <button onClick={this.next}>Next</button>
        </div>
      </div>
    );
  }
}
ReactDOM.render(<Music />, document.getElementById("root"));